package com.company;

import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        LoginCommandModule.class,
        HelloWorldModule.class,
        SystemOutModule.class,
        UserCommandsModule.class
})
interface CommandRouterFactory {
    CommandRouter router();
}
